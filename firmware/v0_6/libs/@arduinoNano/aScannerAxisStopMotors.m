% aNanoStepperStop(obj)
% obj: the arduino controll used for communication
% Return: the state of the stepper enable pin
% % The package send is formed as:
% 0xB0, CMD = 12, 0, 0, 0, 0, 0, suma, 0x0b
% Retorna 2 bytes representando el valor resultante de la conversión.
function ret = aScannerAxisStopMotors(obj)
  v = [176, 11, 0, 0, 0, 0, 0, 0, 11];
  
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
  b0 = srl_read(port, 1);
  b1 = srl_read(port, 1);
  ret =  ((uint16(b1)*256) + uint16(b0));
endfunction
