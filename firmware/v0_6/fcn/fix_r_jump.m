%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  fix_r_jump()
%Function fix_r_jump()
%@end deftypefn

function ret =  fix_r_jump (val_ajust,R_MAX)

%val_ajust = val_ajust / 0.45 * 4; 			%lo pasamos a step
val_ajust = val_ajust / 0.45; 			%lo pasamos a step
val_ajust = round(val_ajust);				%redondeamos los step

while ( rem(R_MAX,val_ajust) != 0)				%Buscara la cantidad par de saltos
	val_ajust = val_ajust+1;
	if(val_ajust>R_MAX)
		disp('No se pudo determinar salto mas cercano');
		val_ajust = 0;
		break;
	end
end
%ret =  val_ajust * 0.45 / 4;				%lo pasamos a grados;
ret =  val_ajust * 0.45;				%lo pasamos a grados;
endfunction
