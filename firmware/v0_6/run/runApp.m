function ret = runApp()
  [dir, name, ext] = fileparts( mfilename('fullpathext') );
  global _test6BasePath = dir;
  global _test6ImgPath = [dir filesep() 'img'];
  addpath([dir filesep() "libs" ]);
  addpath([dir filesep() "fcn" ]);
  addpath([dir filesep() "wnd" ]);
  waitfor(mainDlg().figure);
end
