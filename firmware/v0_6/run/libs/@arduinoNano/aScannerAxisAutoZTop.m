% aScannerAxisAutoZTop(obj, pin)
% obj: the arduino controll used for communication
% delay_step: delay to move steps
% Return: error flag if pass upper limit or if not have been parked.
% % The package send is formed as:
% 0xB0, CMD = 10, 0, 0, 0, 0, 0, suma, 0x0b
% Retorna 2 bytes representando el valor resultante de la conversión.

function [ret,err] = aScannerAxisAutoZTop(obj,delay_step)
  v = [176, 8, delay_step, 0, 0, 0, 0, 0, 11];
  
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  port = obj.serialDev;
  srl_flush(port);
  
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor

  b0 = srl_read(port, 1);
  b1 = srl_read(port, 1);
  b2 = srl_read(port, 1);

  ret =  ((uint16(b1)*256) + uint16(b0));
  err = uint16(b2);  
endfunction
