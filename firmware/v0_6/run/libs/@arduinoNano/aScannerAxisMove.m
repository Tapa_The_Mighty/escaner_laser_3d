% aNanoEjeRSteps(obj, Op, newStep, newDir, delay_step)
% obj: the arduino controll used for communication
% newStep: number of steps to do
% newDir: direction to take, 1=clockwise
% delayStep: delay to take betwen steps in microseconds
% The package send is:
% 0xB0, CMD = 13, newStep, newDir, delay_step, 0, 0, suma, 0x0b

function [ret,err] = aScannerAxisMove(obj, axis_op, newStep, newDir, delayStep)
  doCmd=0;

  if (axis_op == 'r' || axis_op == 'R')
    doCmd = 13;
  end

  if (axis_op == 'z' || axis_op == 'Z')
    doCmd = 12;
  end
  
  if(doCmd!=0)
    v = [176, doCmd, newStep, newDir, delayStep, 0, 0, 0, 11];
    v(8) = 0;

    for i=1:7
        v(8) = v(8) + v(i);
    endfor

    v(8) = mod(v(8), 256);      
    port = obj.serialDev;

    for i=1:9
        srl_write(port, uint8(v(i)));
    endfor

    b0 = srl_read(port, 1);
    b1 = srl_read(port, 1);
    b2 = srl_read(port, 1);
    
    ret =  ((uint16(b1)*256) + uint16(b0));
    err = uint16(b2);

    if(doCmd == 12 && err == 1)
        printf("Error: Para mover eje z primero se debe hacer el home.\n");
    end
    else
    printf("Error: en aScannerAxisMove -> axis_op, debe ser r,R,z o Z.\n");
  end
endfunction
