%   aScannerStartContinuousMode(obj);
%   El sensor detiene el modo continuo de lectura
%   0xB0, CMD = 3, 0, 0, 0, 0, 0, suma, 0x0b

function aScannerSensorStopContinuousMode(obj)
  v = [176, 3, 0, 0, 0, 0, 0, 0, 11];
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
endfunction
