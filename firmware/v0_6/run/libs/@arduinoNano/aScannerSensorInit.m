%   aScannerSensorInit(obj);
%   Inicializa el sensor laser TOF
%   Retorna un bit de error si no puede hacerlo
%   0xB0, CMD = 1, nroPin, tipo 1 - 3, 0, 0, 0, suma, 0x0b

function ret = aScannerSensorInit(obj)
  v = [176, 1, 0, 0, 0, 0, 0, 0, 11];  
  v(8) = 0;
  for i=1:7
    v(8) = v(8) + v(i);
  endfor
  v(8) = mod(v(8), 256);      
  port = obj.serialDev;
  srl_flush(port);
  for i=1:9
    srl_write(port, uint8(v(i)));
  endfor
  b0 = srl_read(port, 1);
  b1 = srl_read(port, 1);
  ret =  ((uint16(b1)*256) + uint16(b0));
  if ret != 0 
    printf("ERROR: No se pudo inicializar sensor. ¿Esta el sensor conectado?.\n");
  end 
endfunction
