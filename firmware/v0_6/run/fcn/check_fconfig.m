
%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  check_fconfig()
%Function check_fconfig()
%@end deftypefn

function x_ret =  check_fconfig (force = false )
         printf("Leyendo archivo de configuración.\n");
         persistent ret = struct ("ok", 0);
         if (ret.ok  == 0 ) || (force == true)
                  if exist("config.oct") == 3
                           ret =load("config.oct");
                           printf("Configuración cargada con exito.\n");
                  else 
                  % Configuracion Default:
							l_path=get_home_directory();
                            printf("ALERTA: no se pudo leer archivo de configuración, se creo el mismo como: config.oct .\n");
                           ret = struct( "ok", 1,								%Variable de existe, I AM
                           "default_EJEZ_ZMAX",{10},				%Altura maxima por defecto
                           "default_EJEZ_STEP",{5},					%Variacion de altura en cada lectura por defecto.
                           "default_ZDELAY_US",{50},					%Delay de microsegundos entre pasos por defecto.
                           "default_EJER_REV",{360},					%Cantidad de angulos maximos por revolución
                           "default_EJER_STEP",{1.125},			%Cantidad de grados a girar en cada lectura por defecto.
                           "default_RDELAY_US",{25},				%Delay de microsegundos entre pasos por defecto.
                           "default_SENSOR_ITR",{10},				%Iteraciones del sensor por lectura
                           "default_SENSOR_T",{45},					%Periodo del sensor por lectura
                           "default_SENSOR_ORIGIN",{1},			%Distancia al centro de la bandeja
                           "default_LOCAL_WORK_OP",{1},		%Carpeta local de trabajo
                           "default_LOCAL_PATH", {l_path}		%Dirección de la carpeta local de trabajo
                               );
                           save config.oct -struct ret;	%Guardamos estructura en archivo. 
                  endif
      endif
      x_ret  = ret ;			%Return Config Struct 
endfunction