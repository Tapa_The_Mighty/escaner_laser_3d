%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  data_to_cloudPoint()
%Function data_to_cloudPoint()
%@end deftypefn

function ret =  data_to_cloudPoint (data,altura_max,rev_max)

col  =length(data(1,:));
row   = length(data(:,1));

cloud = zeros(3,col*row);

tita_cof = rev_max / row;
zeta_cof   = 10 * altura_max / col;     %altura maxima esta en cm 

indice = 0;

## row = revoluciones
## columnas = saltos en z

for i=1:row
      for   j=1:col
            x=data(i,j)*cos(deg2rad(tita_cof*i));
            y=data(i,j)*sin(deg2rad(tita_cof*i));
            z=j*zeta_cof;
            cloud(:,indice + j)=[x,y,z];
      endfor
      indice = indice + col;
endfor
ret =  cloud;
endfunction
