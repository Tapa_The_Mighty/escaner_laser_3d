## -*- texinfo -*-
## @deftypefn  {} {@var{wnd} =} exportDlg ()
##
## Create and show the dialog, return a struct as representation of dialog.
##
## @end deftypefn
function wnd = exportDlg()
  exportDlg_def;
  wnd = show_exportDlg();
end
