## -*- texinfo -*-
## @deftypefn  {} {@var{wnd} =} configDlg ()
##
## Create and show the dialog, return a struct as representation of dialog.
##
## @end deftypefn
function wnd = configDlg()
  configDlg_def;
  wnd = show_configDlg();
end
