## -*- texinfo -*-
## @deftypefn  {} {@var{ret} =} test6 ()
##
## Create and show the main dialog, return a struct as representation of dialog.
##
## @end deftypefn
function ret = test6()
  [dir, name, ext] = fileparts( mfilename('fullpathext') );
  global _test6BasePath = dir;
  global _test6ImgPath = [dir filesep() 'img'];
  addpath( [dir filesep() "img" ]);
  addpath( [dir filesep() "fcn" ]);
  addpath( [dir filesep() "libs" ]);
  addpath( [dir filesep() "wnd" ]);
  mainDlg();
end
