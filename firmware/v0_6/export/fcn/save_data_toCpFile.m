%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  save_data_toCpFile()
%Function save_data_toCpFile()
%@end deftypefn

function ret =  save_data_toCpFile (data,fname,fpath)
          path = strcat(fpath,fname);
          fid = fopen (path, "w");
          if (fid != -1)
                fprintf(fid, "#ASCII \n");
                for i=1:columns(data)
       				for j=1:3
          				fprintf(fid,"%f ", data(j,i));
       				end
      				fprintf(fid, '\n');
    			end
				fclose(fid);          
                ret = 1;
           else
                 ret = 0;
      endif
endfunction