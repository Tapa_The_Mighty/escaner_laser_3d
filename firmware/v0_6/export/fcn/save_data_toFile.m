	% -*- texinfo -*-
% @deftypefn {Function File} {@var{ret} =}  save_data_toFile()
% Function save_data_toFile(data,sample_time,buffer,step)
%@end deftypefn
                   
function ret =  save_data_toFile (data,fname,fpath,grad_step,mm_step)
          path = strcat(fpath,fname);
          fid = fopen (path, "w");
          if (fid != -1)
                fprintf(fid, "%% ESCANER LASER 3D by Andres Tapari 2021 \n");
                fprintf(fid, "%%|--> System Info: \n");
                fprintf(fid, "%%|        |--> File created: %s \n",date);
                fprintf(fid, "%%|        |--> Octave Version: %s \n",OCTAVE_VERSION ());
                fprintf(fid, "%%|        |--> System: %s \n",computer ());                  
                fprintf(fid, "%%|--> Experiment Data: \n");
                fprintf(fid, "%%|        |--> Rotacion de bandeja por paso: %f [º] \n",grad_step);
                fprintf(fid, "%%|        |--> Elevacion de carro por paso: %f [mm]\n",mm_step);
                fprintf(fid, "%%|        |--> Tamaño de buffer: [%i , %i] \n",size(data));
                fprintf(fid, "%%|--> Data Start:{ \n\n");
                for i=1:size(data, 1)
                      fprintf(fid, '%i ', data(i,:));
                      fprintf(fid, '\n');
                 end
                 fprintf(fid, "\n%%}End of Data");
                 fclose(fid);          
                 ret = 1;
           else
                 ret = 0;
      endif
endfunction

