#include "Arduino.h"
#include "scannerpinout.h"
#include <string.h>
#include <Wire.h>
#include <VL53L0X.h>


#include "serialProtocol.h"

//static Servo servos[4];

VL53L0X * sensor;

bool MACHINE_RUN = true;                  //allow to disable motors any time
bool HOME = false;                        //enable motors after the home function is called
bool SENSOR_INIT = false;

const long int max_step_in_z =  224000;   //Max H in z axis7500*32

int long z_counter;                       //Counter for Z axis
int long r_counter;                       //Counter for R Axis

/*
   aScannerSensorInit(obj);
   Inicializa el sensor laser TOF
   Retorna un bit de error si no puede hacerlo
   0xB0, CMD = 1, nroPin, tipo 1 - 3, 0, 0, 0, suma, 0x0b
*/

void doCMD01(protocolData *c) {
  unsigned char b0, b1;
  int err = 0;
  if (!SENSOR_INIT) {
    sensor = new VL53L0X();
    if (sensor->init()) {
      err = 0;
      SENSOR_INIT = true;
    }
    else {
      err = 1;
    }
  }
  b0 = err & 0xFF;
  err = err >> 8;
  b1 = err & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}

/*
   aScannerSensorStartContinuousMode(obj):
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 2, delayTime_ms, 0, 0, 0, 0, suma, 0x0b
   Retorna 2 bytes representando el valor resultante de la conversión.
*/
void doCMD02(protocolData *c)
{
  unsigned int val = 0;
  unsigned char b0, b1;

  if (c->data[0] != 0) {
    sensor->startContinuous(c->data[0]);
  }
  else {
    sensor->startContinuous(); //modo default
  }
  
  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}

/***
   aScannerStopContinuousMode(obj)
   obj: corresponde con el que se establece la comunicación.
   Detiene el modo continuo del sensor
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 3, nroPin, 0, 0, 0, 0, suma, 0x0b
   Retorna 1 bytes representando el valor presente.
*/

void doCMD03(protocolData *c)
{
  sensor->stopContinuous();
}

/*
   aScannerSensorReadContinuousRange(obj)
   retorna una lectura en modo continuo
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 4, 0, 0, 0, 0, 0, suma, 0x0b
*/
void doCMD04(protocolData *c)
{
  unsigned int val = 0;
  unsigned char b0, b1;

  val = sensor->readRangeContinuousMillimeters();

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}

/***
   aScannerSensorReadSingleRange(obj)
   obj: corresponde con el que se establece la comunicación.
   Lee el sensor en modo unico.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 5, nroPin, val, 0, 0, 0, suma, 0x0b
*/

void doCMD05(protocolData *c)
{
  unsigned int val = 0;
  unsigned char b0, b1;

  //Modo alta presición
  sensor->setMeasurementTimingBudget(200000);
  val = sensor->readRangeSingleMillimeters();

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}

/*
   aScannerSensorReadSingleIterativeRange(obj, iteration)
   obj: corresponde con el que se establece la comunicación.
   iterartion: cantidad de iteraciones a hacer.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 6, nroServo, nroPin, 0, 0, 0, suma, 0x0b
*/

void doCMD06(protocolData *c)
{
  unsigned int val = 0;
  unsigned char b0, b1, b2;
  float prom = 0;
  //Modo alta presición
  sensor->setMeasurementTimingBudget(200000);
  for (int i = 0; i < c->data[0]; i++) {
    prom = prom + sensor->readRangeSingleMillimeters();
  }
  val = (unsigned int)(prom / c->data[0]);


  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  Serial.write(b0);
  Serial.write(b1);
}

/***
   aNanoServoDetach(obj, nroServo)
   obj: corresponde con el que se establece la comunicación.
   nroServo: índice del servo a utilizar. Valores válidos van de 0 a 3 inclusive.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 7, nroServo, 0, 0, 0, 0, suma, 0x0b
*/

void doCMD07(protocolData *c)
{
  unsigned int val = 0;
  unsigned char b0, b1, b2;
  float prom = 0;

  for (int i = 0; i < c->data[0]; i++) {
    prom = prom + sensor->readRangeContinuousMillimeters();
  }
  val = (unsigned int)(prom / c->data[0]);

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}

void stepper_move(int step_pin, int microdelay) {
  digitalWrite(step_pin, HIGH);
  delayMicroseconds(microdelay);
  digitalWrite(step_pin, LOW);
  delayMicroseconds(microdelay);

}

void doCMD08(protocolData * c)
{
  unsigned int val = 0;
  unsigned char b0, b1, b2;
  int err = 0;
  int dist = 0;

  long int step_counter = 0;

  if (HOME) {
    digitalWrite(STEPPERS_ENABLE, 0);
    digitalWrite(EJE_Z_DIR, LOW);
    dist = sensor->readRangeContinuousMillimeters();
    while (dist < 500) {
      stepper_move(EJE_Z_STEP, c->data[0]);
      step_counter++;
      dist = sensor->readRangeContinuousMillimeters();
      if (step_counter >= max_step_in_z) {
        err = 1;
        break;
      }
    } //fin del while
    val = (int)step_counter * 0.004;
  } //fin del if(home)
  else {
    err = 1;
  }
  digitalWrite(STEPPERS_ENABLE, 1);

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  b2 = err & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
  Serial.write(b2);
}

void doCMD09(protocolData * c)
{
  // sensor_clon = new VL53L0X;
  // sensor_clon->init();
  // sensor_clon->
  //do nothing

  //do nothing
}

/***
   aScannerAxisHome(obj,delay_step)
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 10, 0, 0, 0, 0, 0, suma, 0x0b
*/

void doCMD0A(protocolData * c)
{
  unsigned int val = 0;
  unsigned char b0, b1;
  int err = 0;

  long int step_counter = 0;
  if (!MACHINE_RUN) {
    MACHINE_RUN = true;
  }


  digitalWrite(STEPPERS_ENABLE, 0);
  digitalWrite(EJE_Z_DIR, HIGH);

  HOME = true;

  val = digitalRead(EJE_Z_ENDSTOP_PIN);

  while (val && MACHINE_RUN) {

    stepper_move(EJE_Z_STEP, c->data[0]);
    val = digitalRead(EJE_Z_ENDSTOP_PIN);
    step_counter++;
    if (step_counter > max_step_in_z) { //si es true el motor esta desconectado
      err = 1;
      HOME = false;
      break;
    }
  }

  digitalWrite(STEPPERS_ENABLE, 1);
  //Reinicia los contadores

  z_counter = 0;
  r_counter = 0;

  b0 = err & 0xFF;
  err = err >> 8;
  b1 = err & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
}


/***
   aNanoStepperStop(obj)
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 11, 0, 0, 0, 0, 0, suma, 0x0b
*/

void doCMD0B(protocolData * c)
{
  unsigned int val = 0;
  unsigned char b0, b1;

  digitalWrite(STEPPERS_ENABLE, 1);
  MACHINE_RUN = false;
  val = MACHINE_RUN;

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  Serial.write(b0);
  Serial.write(b1);
}

/***
   aScannerAxisMove(obj,z,step,dir,delay_step)
   seto: numero de pasos a dar
   dir: direccion a girar, 1 = clockwise
   delay_step: velocidad de giro
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 12, step, dir, delay_step, 0, 0, suma, 0x0b
*/


void doCMD0C(protocolData * c)
{
  unsigned int val = 0;
  unsigned char b0, b1, b2;
  bool err = false;

  if (!MACHINE_RUN) {
    MACHINE_RUN = true;
  }

  digitalWrite(EJE_Z_DIR, c->data[1]);
  digitalWrite(STEPPERS_ENABLE, 0);

  // for (int i = 200; i >= 0 ; i--) {
  int counter = c->data[0] * 32;
  int delay_time = c->data[2];
  int i = 0;


  if (HOME == true) {
    switch (c->data[1]) {
      case 0: //Sube
        while ((i < counter) && (MACHINE_RUN) && (z_counter < max_step_in_z)) {
          stepper_move(EJE_Z_STEP, delay_time);
          i = i + 1;
          z_counter = z_counter + 1;
        }
        break;
      case 1:
        val = digitalRead(EJE_Z_ENDSTOP_PIN);
        while (val == 1 && i < counter && MACHINE_RUN) {
          stepper_move(EJE_Z_STEP, delay_time);
          i++;
          z_counter--;
          val = digitalRead(EJE_Z_ENDSTOP_PIN);
        }
        break;
      default:
        //do nothing
        break;
    }
    val = 0;            //devuelve el contador en la posición z
  }//fin del if home
  else {
    err = 1;
    val = 1;
  }

  if (err) {
    digitalWrite(STEPPERS_ENABLE, 1);
  }

  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  b2 = err & 0xFF;
  
  Serial.write(b0);
  Serial.write(b1);
  Serial.write(b2);
}

/***
   aScannerAxisMove(obj,r,step,dir,delay_step)
   seto: numero de pasos a dar
   dir: direccion a girar, 1 = clockwise
   delay_step: velocidad de giro
   obj: corresponde con el que se establece la comunicación.
   El paquete de datos, para pines digitales, se construye como:
   0xB0, CMD = 12, step, dir, delay_step, 0, 0, suma, 0x0b
*/

void doCMD0D(protocolData * c)
{
  unsigned int val = 0;
  unsigned char b0, b1, b2;

  if (!MACHINE_RUN) {
    MACHINE_RUN = true;
  }

  digitalWrite(EJE_R_DIR, c->data[1]);
  digitalWrite(STEPPERS_ENABLE, 0);

  int counter = c->data[0] * 32;
  int delay_time = c->data[2];
  int i = 0;

  while ((i < counter) && (MACHINE_RUN) ) {
    stepper_move(EJE_R_STEP, delay_time);
    i = i + 1;
    r_counter = r_counter + 1;
    if (r_counter > 25600) {
      r_counter = 1;  //Si el plato da una vuelta reinicia el contador
    }
  }

  b2 = 0;
  b0 = val & 0xFF;
  val = val >> 8;
  b1 = val & 0xFF;
  b2 = b2 & 0xFF;

  Serial.write(b0);
  Serial.write(b1);
  Serial.write(b2);
}



const doCMD cmdActionList[17] =
{
  doCMD01, //  0
  doCMD02, //  1
  doCMD03, //  2
  doCMD04, //  3
  doCMD05,
  doCMD06,
  doCMD07,
  doCMD08,
  doCMD09,
  doCMD0A,  //VlcRead
  doCMD0B,  //EjeZHome
  doCMD0C,  //StepperStop
  doCMD0D
};

//
// Detecta el encabezado
//

void stageHeader(protocolData * b, unsigned char val)
{
  if (val == 0xB0)
  {
    b->crc = 0xB0;
    b->dataInx = 0;
    b->nextState = STATE_CMD;
  }
  else
    b->nextState = STATE_HEADER;
}
//
// Carga CMD
//
void stageCMD(protocolData * b, unsigned char val)
{
  if ((val >= FIRST_CMD) && (val <= LAST_CMD))
  {
    b->crc += val;
    b->cmd = val;
    b->nextState = STATE_DATA;
  }
  else
    b->nextState = STATE_HEADER;
}

//
// Carga payload
//
void stageDATA(protocolData * b, unsigned char val)
{
  b->data[b->dataInx] = val;
  b->crc += val;
  b->dataInx ++;
  if (b->dataInx == 5)
    b->nextState = STATE_CRC;
}

//
// verifica CRC (ojo, no incluye el marcador de fin de paquete 0x0B
//
void stageCRC(protocolData * b, unsigned char val)
{
  if (b->crc == val)
    b->nextState = STATE_FOOTER;
  else
    b->nextState = STATE_HEADER;
}

//
// Verifica fin de paquete y ejecuta la orden asociada.
//
void stageFOOTER(protocolData * b, unsigned char val)
{
  if (val == 0x0B)
  {
    if ((b->cmd >= FIRST_CMD) && (b->cmd <= LAST_CMD))
      cmdActionList[b->cmd - FIRST_CMD](b);
  }
  b->nextState = STATE_HEADER;
}


const stageFunc allStages[5] =
{
  stageHeader, // 0
  stageCMD,    // 1
  stageDATA,   // 2
  stageCRC,    // 3
  stageFOOTER  // 4
};

void initProtocol(protocolData * p)
{
  memset(p, 0, sizeof(protocolData));
}

void decodeProtocol(protocolData * p, unsigned char newVal)
{
  allStages[p->nextState](p, newVal);
}
