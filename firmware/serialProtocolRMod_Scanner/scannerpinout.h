#ifndef SCANNERPINOUT
#define SCANNERPINOUT

//Definiciones del MOD para Escaner 3D
#define EJE_Z_ENDSTOP_PIN 11

#define EJE_Z_DIR 7
#define EJE_Z_STEP 4

#define EJE_R_DIR 5
#define EJE_R_STEP 2

#define STEPPERS_ENABLE 8

#endif
