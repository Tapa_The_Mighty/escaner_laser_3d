#include <Wire.h>
#include "serialProtocol.h"
#include "scannerpinout.h"
protocolData serIn;

void setup() {
  Wire.begin();

  Serial.begin(115200);
  initProtocol(&serIn);

  pinMode(EJE_Z_ENDSTOP_PIN, INPUT_PULLUP);
  pinMode(STEPPERS_ENABLE, OUTPUT);
  pinMode(EJE_Z_DIR, OUTPUT);
  pinMode(EJE_Z_STEP, OUTPUT);
  
  pinMode(EJE_R_DIR, OUTPUT);
  pinMode(EJE_R_STEP, OUTPUT);
  
  digitalWrite(STEPPERS_ENABLE, 1); //Comienza con los motores apagados
}

void loop() {
  // put your main code here, to run repeatedly:
  ;
}

void serialEvent() {
  while (Serial.available())
    decodeProtocol(&serIn, (unsigned char)Serial.read());
}
