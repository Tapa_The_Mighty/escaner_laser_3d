#ifndef SERIALPROTOCOL
#define SERIALPROTOCOL

#define STATE_HEADER 0
#define STATE_CMD    1
#define STATE_DATA   2
#define STATE_CRC    3
#define STATE_FOOTER 4

#define FIRST_CMD 0x01

#define CMD_SENSOR_INIT           0x01    //<- Sensor control start here
#define CMD_SENSOR_START_C_MODE   0x02
#define CMD_SENSOR_STOP_C_MODE    0x03
#define CMD_SENSOR_READ_C_MODE    0x04
#define CMD_SENSOR_READ_S_MODE    0x05
#define CMD_SENSOR_READ_I_S_MODE  0x06
#define CMD_SENSOR_NOTHING_1      0x07
#define CMD_AUTO_Z_TOP            0x08
#define CMD_SENSOR_NOTHING_3      0x09
#define CMD_AXIS_HOME             0x0A    //<- Axis control start here
#define CMD_AXIS_MOTORS_STOP      0x0B
#define CMD_EJEZ_WRITE            0x0C
#define CMD_EJER_WRITE            0x0D
#define LAST_CMD CMD_EJER_WRITE

#define bit00 0x01
#define bit01 0x02
#define bit02 0x04
#define bit03 0x08
#define bit04 0x10
#define bit05 0x20
#define bit06 0x40
#define bit07 0x80

typedef struct
{
  unsigned char cmd;
  unsigned char data[5];
  unsigned char dataInx;
  unsigned char crc;
  unsigned char nextState;
} protocolData;

typedef void (*stageFunc)(protocolData *, unsigned char);

void stageHeader(protocolData* b, unsigned char val);
void stageCMD(protocolData* b, unsigned char val);
void stageDATA(protocolData* b, unsigned char val);
void stageCRC(protocolData* b, unsigned char val);
void stageFOOTER(protocolData* b, unsigned char val);

typedef void (*doCMD)(protocolData*);

void doCMD01(protocolData *);
void doCMD02(protocolData *);
void doCMD03(protocolData *);
void doCMD04(protocolData *);
void doCMD05(protocolData *);
void doCMD06(protocolData *);
void doCMD07(protocolData *);
void doCMD08(protocolData *);
void doCMD09(protocolData *);
void doCMD0A(protocolData *);
void doCMD0B(protocolData *);
void doCMD0C(protocolData *);
void doCMD0D(protocolData *);


void initProtocol(protocolData *);
void decodeProtocol(protocolData *, unsigned char newVal);

void stepper_move(int step_pin, int microdelay);

#endif
